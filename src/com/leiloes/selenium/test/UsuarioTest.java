package com.leiloes.selenium.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.leiloes.selenium.test.pages.NovoUsuarioPage;
import com.leiloes.selenium.test.pages.UsuariosPage;

public class UsuarioTest {
	
	private RemoteWebDriver driver;
	private UsuariosPage usuarios;

	@Before
	public void init() {
		System.setProperty("webdriver.chrome.driver", "\\D:\\dev\\IDE\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.get("http://localhost:8080/apenas-teste/limpa");
		
		usuarios = new UsuariosPage(driver);
	}
	
	@After
	public void finish(){
		driver.close();
	}
	
    @Test
    public void deveCadastrarNovoUsuarioEExibirNaLista() {
		String nomeCadastro = "Adriano Xavier";
		String emailCadastro = "axavier@empresa.com.br";
		
		usuarios.visita();
		usuarios.novo().cadastra(nomeCadastro, emailCadastro);

        assertTrue(usuarios.existeUsuario(nomeCadastro, emailCadastro));
    }
    
    @Test
    public void deveRetornarErroAoCadastrarUsuarioVazio() {
		
		usuarios.visita();
		NovoUsuarioPage novoUsuario = usuarios.novo();
		novoUsuario.cadastra("", "");
		
        assertTrue(novoUsuario.existeErro("Nome obrigatorio!"));
        assertTrue(novoUsuario.existeErro("E-mail obrigatorio!"));
    }
    
    @Test
    public void deveExcluirUltimoUsuarioDaLista() {
		String nomeCadastro = "Excluir esse usuario";
		String emailCadastro = "excluir@esse.com.br";
		
		usuarios.visita();
		usuarios.novo().cadastra(nomeCadastro, emailCadastro);
		
		usuarios.excluirUltimoUsuario();
		
		assertFalse(usuarios.existeUsuario(nomeCadastro));
    }
    
    @Test
    public void deveEditarUltimoUsuarioDaLista() {
    	String nomeCadastro = "Editar esse usuario";
    	String emailCadastro = "editar@esse.com.br";
    	
    	usuarios.visita();
    	usuarios.novo().cadastra(nomeCadastro, emailCadastro);
    	
    	String nomeEditado = "Editado";
    	String emailEditado = "editado@esse.com.br";
    	usuarios.alterarUltimo().altera(nomeEditado, emailEditado);
    	
    	assertTrue(usuarios.existeUsuario(nomeEditado, emailEditado));
    }
}