package com.leiloes.selenium.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class AlteraUsuarioPage {
	
	private RemoteWebDriver driver;

	public AlteraUsuarioPage(RemoteWebDriver driver) {
		this.driver = driver;
	}

	public void altera(String nome, String email) {
		WebElement txtNome = driver.findElement(By.name("usuario.nome"));
        WebElement txtEmail = driver.findElement(By.name("usuario.email"));

        txtNome.clear();
        txtNome.sendKeys(nome);
        txtEmail.clear();
        txtEmail.sendKeys(email);

        txtNome.submit();
	}

	public boolean existeErro(String erro) {
		return existeTexto(erro);
	}
	
	public boolean existeTexto(String texto) {
		return driver.getPageSource().contains(texto);
	}
}
