package com.leiloes.selenium.test.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

public class UsuariosPage {

	private RemoteWebDriver driver;

	public UsuariosPage(RemoteWebDriver driver) {
		this.driver = driver;
	}
	
	public void visita() {
        driver.get("localhost:8080/usuarios");
    }

	public NovoUsuarioPage novo() {
		driver.findElement(By.linkText("Novo Usuário")).click();
		return new NovoUsuarioPage(driver);
	}
	
	public AlteraUsuarioPage alterar(int posicao) {
		driver.findElements(By.linkText("editar")).get(posicao-1).click();
		return new AlteraUsuarioPage(driver);
	}

	public boolean existeUsuario(String nome, String email) {
		return existeTexto(nome) && existeTexto(email);
	}
	
	public boolean existeUsuario(String nome) {
		return existeTexto(nome);
	}

	public boolean existeErro(String erro) {
		return existeTexto(erro);
	}
	
	public boolean existeTexto(String texto) {
		return driver.getPageSource().contains(texto);
	}

	public void excluirPrimeiro() {
		deletaUsuarioNaPosicao(1);
	}
	
	public void excluirUltimoUsuario() {
		deletaUsuarioNaPosicao(driver.findElements(By.tagName("button")).size());
	}
	
	public void deletaUsuarioNaPosicao(int posicao) {
		driver.findElements(By.tagName("button")).get(posicao-1).click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public AlteraUsuarioPage alterarUltimo() {
		return alterar(driver.findElements(By.linkText("editar")).size());
	}
}
