package com.leiloes.selenium.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.leiloes.selenium.test.pages.LeiloesPage;
import com.leiloes.selenium.test.pages.NovoLeilaoPage;
import com.leiloes.selenium.test.pages.UsuariosPage;

public class LeiloesTest {

    private RemoteWebDriver driver;
    private LeiloesPage leiloes;
	private UsuariosPage usuarios;

    @Before
	public void init() {
		System.setProperty("webdriver.chrome.driver", "\\D:\\dev\\IDE\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.get("http://localhost:8080/apenas-teste/limpa");
		
        leiloes = new LeiloesPage(driver);
        
        usuarios = new UsuariosPage(driver);
        usuarios.visita();
        usuarios.novo().cadastra("Paulo Henrique", "paulo@henrique.com");
    }

	@After
	public void finish(){
		usuarios.visita();
		usuarios.excluirUltimoUsuario();
		driver.close();
	}

    @Test
    public void deveCadastrarUmLeilao() {

        leiloes.visita();
        NovoLeilaoPage novoLeilao = leiloes.novo();
        novoLeilao.preenche("Geladeira", 123, "Paulo Henrique", true);

        assertTrue(leiloes.existe("Geladeira", 123, "Paulo Henrique", true));

    }

    @Test
    public void deveRetornarErroAoCadastrarLeilaoVazio() {
		
		leiloes.visita();
		NovoLeilaoPage novoLeilao = leiloes.novo();
		novoLeilao.preenche("", 0, "Paulo Henrique", true);
		
        assertTrue(novoLeilao.existeErro("Nome obrigatorio!"));
        assertTrue(novoLeilao.existeErro("Valor inicial deve ser maior que zero!"));
    }
}
