package com.leiloes.selenium.test;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.leiloes.selenium.test.pages.DetalhesDoLeilaoPage;
import com.leiloes.selenium.test.pages.LeiloesPage;

public class LanceTest {
	private RemoteWebDriver driver;
    private LeiloesPage leiloes;

    @Before
    public void inicializa() {
    	System.setProperty("webdriver.chrome.driver", "\\D:\\dev\\IDE\\chromedriver.exe");
        this.driver = new ChromeDriver();

        driver.get("http://localhost:8080/apenas-teste/limpa");
        
        leiloes = new LeiloesPage(driver);
        
        new CriadorDeCenarios(driver)
	        .umUsuario("Paulo Henrique", "paulo@henrique.com")
	        .umUsuario("José Alberto", "jose@alberto.com")
	        .umLeilao("Geladeira", 100, "Paulo Henrique", false);
    }
    
    @After
    public void finaliza(){
    	driver.close();
    }

    @Test
    public void deveFazerUmLance() {

        DetalhesDoLeilaoPage lances = leiloes.detalhes(1);

        lances.lance("José Alberto", 150);

        assertTrue(lances.existeLance("José Alberto", 150));
    }
}
