package com.leiloes.selenium.test;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.leiloes.selenium.test.pages.LeiloesPage;
import com.leiloes.selenium.test.pages.UsuariosPage;

class CriadorDeCenarios {

    private RemoteWebDriver driver;

    public CriadorDeCenarios(RemoteWebDriver driver) {
        this.driver = driver;
    }

    public CriadorDeCenarios umUsuario(String nome, String email) {
        UsuariosPage usuarios = new UsuariosPage(driver);
        usuarios.visita();
        usuarios.novo().cadastra(nome, email);

        return this;
    }

    public CriadorDeCenarios umLeilao(String produto, double valor, String usuario, boolean usado) {
        LeiloesPage leiloes = new LeiloesPage(driver);
        leiloes.visita();
        leiloes.novo().preenche(produto, valor, usuario, usado);

        return this;
    }

}
