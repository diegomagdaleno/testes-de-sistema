package com.leiloes.selenium.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteAutomatizado {

    public static void main(String[] args) {
    	// abre Chrome
    	System.setProperty("webdriver.chrome.driver", "\\D:\\dev\\IDE\\chromedriver.exe");
    	System.out.println(System.getProperty("webdriver.chrome.driver"));
    	WebDriver driver = new ChromeDriver();

        // acessa o site do google
        driver.get("http://www.bing.com/");

        // digita no campo com nome "q" do google
        WebElement campoDeTexto = driver.findElement(By.name("q"));
        campoDeTexto.sendKeys("Caelum");

        // submete o form
        campoDeTexto.submit();
    }
}